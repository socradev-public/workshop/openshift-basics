{ 
  pkgs ? import <nixpkgs> {} }:pkgs.mkShell {
  shellHook =
  ''
    echo "Welcome!"
    source /usr/share/bash-completion/bash_completion || true
  '';
  
  packages = with pkgs; [
    bashInteractive
    bash-completion
    jq
    kubectl
    openshift
    kubernetes-helm
    k9s
  ];
}
