# Tools
The prefered way to get your env up and running for this workshop is using `nix-shell`, as described in `README.md`.

This `TOOLS.md` file should help you to set up the environment if you don't want to use _Nix_, or if you're setting up your own environment after the workshop.

# Installation methods
First, have a look in your favorite package manager - changes are high that you might be able to just install it from there.
If not, you have to install the binary manually:
- Download release
- Unarchive archive
- Make executable executable
- Move executable into your `$PATH`
- Configure completions for your shell
- Keep everything updated, always

Or just follow the install guide if applicable.

## Required tools
- `oc` OpenShift Client: https://github.com/okd-project/okd/releases/download/4.15.0-0.okd-2024-02-23-163410/openshift-client-linux-4.15.0-0.okd-2024-02-23-163410.tar.gz ([Releases](https://github.com/okd-project/okd/releases/latest))

## Mentioned tools
- `k9s`: https://k9scli.io/topics/install/
- `helm`: https://helm.sh/docs/intro/install/
- `tilt`:  https://docs.tilt.dev/install.html
- `kind`: https://kind.sigs.k8s.io/docs/user/quick-start/#installation
