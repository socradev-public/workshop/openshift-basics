# Directly applying pods
- What happens when they get deleted?
- How can we update the image?
- Why are some pods crashing?

# Commands
```bash
# Redirect kubernetesPort to localhost:localPort
oc port-forward pod/abc localPort:kubernetesPort`

# Redirect kubernetesPort to localhost:kubernetesPort
oc port-forward pod/abc kubernetesPort`

# Redirect kubernetesPort to localhost:randomPort
oc port-forward pod/abc :kubernetesPort`
```