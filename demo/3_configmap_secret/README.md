# What we'll see here
- Data from ConfigMaps and Secrets can be used as env vars
  - `valueFrom` loads the value for a key in a ConfigMap / Secret into a specified env variable
  - `envFrom` loads **all** data from a ConfigMap / Secret into variables corresponding to their keys
- Updating data can have unexpected results for running pods - or not

# Create ConfigMaps and Secrets from command line
## Create ConfigMap
```bash
# From specified values
oc create configmap configmapname --from-literal=a=b --dry-run=client -oyaml

# From file
oc create configmap configmapname --from-file=README.md --from-file=ANOTHER.md=README.md --dry-run=client -oyaml
```

## Create Secret
```bash
# From specified values
oc create secret generic configmapname --from-literal=a=b --dry-run=client -oyaml

# From file
oc create secret generic configmapname --from-file=README.md --from-file=ANOTHER.md=README.md --dry-run=client -oyaml
```
