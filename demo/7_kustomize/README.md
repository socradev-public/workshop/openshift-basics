# Compare output between test and prod overlays
```bash
oc kustomize overlays/test

oc kustomize overlays/prod
```

For full docs, visit [kustomize.io](https://kustomize.io/).
