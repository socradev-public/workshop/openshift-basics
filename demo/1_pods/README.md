# Directly applying pods
- What happens when they get deleted?
- How can we update the image?
- Why are some pods crashing?

# Commands
```
oc logs -f podname # follow live logs
oc logs -p podname # show logs of last container run, before restart / crash
oc logs --tail=N podname # restrict output to last N lines to avoid spamming your terminal
```