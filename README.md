# OpenShift Basics

![socra.dev Logo - an owl inspired by Athena](assets/socradev.png)

Welcome to the _OpenShift Basics_ workshop by [socradev GmbH](https://socra.dev)!

The presentation deck was made with [slides.com](https://slides.com) using [revealjs](https://revealjs.com/) under the hood.

You can either open `slides/index.html` after cloning this repository, or go to https://socradev-public.gitlab.io/workshop/openshift-basics/

## Want to hear it yourself?
Get in touch with us at [office@socra.dev](mailto:office@socra.dev) - we would be happy to present this to you as well, or even create a tailor made training for you!

If you've seen theses slides without ever hearing from us, please let us know, and nudge your presenter to respect [CC BY-SA](LICENSE).

## Getting started
This workshop uses [nix-shell](https://www.youtube.com/watch?v=0ulldVwZiKA) (shoutout to Victor!) to set up a reproducible dev environment.
If you don't want to use that, have a look at [TOOLS.md](TOOLS.md) to reproduce it yourself :)

```bash
git clone https://gitlab.com/socradev-public/workshop/openshift-basics.git
cd openshift-basics

./nix-install.sh
nix-shell
```

## Contents
- Kubernetes Distros
  - Core Components
  - OpenShift vs Kubernetes
- How to interact with the cluster
- Resources
  - Workloads
  - Configs
  - Storage
  - Networking
  - RBAC
- 12 Factor Apps
- Manifest management
  - Kustomize
  - Helm
- Deployment management
- Dev tooling
- Further reading

# Language
While the README.md might give a different impression, the workshop (or at least the slides) are held in German; translations are welcome!


# Time budget
The slides are packed with information, broken up by some small practical examples.

We're targeting half a day, with time left for questions / discussions, and an optional exercise at the end.

Good luck with keeping up :)


# Contribution
Yes please - just create a Merge Request; the [license is CC BY-SA](LICENSE)!
